Meta:
@test Access Game Website

Scenario: Website is accessible
Given that a user accesses tictactoe website
Then the tictactoe game is visible

Scenario: The Game is not marked at the beginning
Given that a user accesses tictactoe website
When no square is marked
Then the tictactoe game is at ready state