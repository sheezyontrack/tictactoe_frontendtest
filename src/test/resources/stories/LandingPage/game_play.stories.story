Meta:
@test Game Play

Scenario: First click should be marked with X
Given that a user accesses tictactoe website
When the tictactoe game is visible
When user makes a first click
Then the square field should be marked with X

Scenario: Second click should be marked with O
Given that a user accesses tictactoe website
When the tictactoe game is visible
When user makes a first click
When user makes a second click
Then the square field should be marked with O

Scenario: All rows and columns should be clickable provided that there is no winner or loser yet
Given that a user accesses tictactoe website
When user clicks on row 3 column 2
When user clicks on row 3 column 1
When user clicks on row 3 column 3
When user clicks on row 2 column 2
When user clicks on row 2 column 1
When user clicks on row 2 column 3
When user clicks on row 1 column 3
When user clicks on row 1 column 3
When user clicks on row 1 column 2
When user clicks on row 1 column 1
Then all rows and colums are clickable

Scenario: A Loser when the boxes in diagonals are not marked with X
Given that a user accesses tictactoe website
When the tictactoe game is visible
When user clicks on row 1 column 1
When user clicks on row 2 column 2
When user clicks on row 3 column 3
When all diagonals matrices are not marked with X
Then there is no winner

Scenario: A Winner when all diagonals are marked with X
Given that a user accesses tictactoe website
When the tictactoe game is visible
When user clicks on row 1 column 1
When user clicka on row 1 column 2
When user clicks on row 2 column 2
When user clicks on row 2 column 3
When user clicks on row 3 column 3
When all diagonal squares are maked with X
Then there is a winner

Scenario: Reversing to a previous game state is possible
Given that a user accesses tictactoe website
When user clicks on row 3 column 2
When user clicks on row 3 column 1
When user clicks on row 3 column 3
When user clicks on row 2 column 2
When user clicks on row 2 column 1
When user goes back to the previous game state
Then returning to a previous state is possible

Scenario: Game restart is possible
Given that a user accesses tictactoe website
When user clicks on row 3 column 2
When user clicks on row 3 column 1
When user clicks on row 3 column 3
When user clicks on row 2 column 2
When user clicks on row 2 column 1
When user clicks on game restart
When returning to a previous state is possible
When user clicks on game restart
Then no square is marked
